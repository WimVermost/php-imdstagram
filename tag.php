<?php

include_once('classes/Photo.class.php');
include_once('classes/User.class.php');
//$Photo = new Photo();
session_start();
if(!isset($_SESSION['id'])){
    header('location: login.php');
}
if(!isset($_GET['tag'])){
    header('location: index.php');
}
$tagname = "#".$_GET['tag'];
?><!doctype html>
<html lang="en">
    <head>
        <title><?php echo"#".$_GET['tag'] ?> - IMDstagram</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.png" sizes="16x16 32x32" type="image/png">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/dropzone.min.css">
        <link rel="stylesheet" href="css/cssgram.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/jquery-2.2.3.min.js" defer></script>
        <script src="js/typeahead.min.js" defer></script>
        <script src="js/imd.js" defer></script>
    </head>
    <body>
    <?php include "includes/nav.php"; ?>

    <?php
    $Photo = new Photo();
    $Photo->Userid = $_SESSION['id'];
    $Loads = $Photo->getTag($tagname);

    $count = $Photo->getTagRows($tagname);
    ?>
    <div class="tagTitle">
        <h1><?php echo $tagname;?></h1>
        <div class="posts">
            <h2><?php echo $count;?></h2>
            <p>&nbsp;&nbsp;posts</p>
        </div>

    </div>
    <div class="profile_images">
        <?php foreach($Loads as $x) { ?>
        <?php $filter = $x['filter']; ?>
        <div class="profile-user-img-block<?php if(!empty($filter)){echo" $filter";} ?>" <?php echo "id='" . $x['uploadId'] . "'"?>>
            <img src="images/useruploads/<?php echo $x['photo'];?>" alt="" >
        </div>

        <?php
        }
        ?>
    </div>
    <?php include_once "includes/footer.php"?>
    <?php include_once "includes/single_post.php"?>
</body>
