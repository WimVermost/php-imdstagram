<?php
    include_once "classes/User.class.php";
    session_start();

    if(isset($_POST['registerSubmit'])){

        $user = new User();
        $username = $user->Username = htmlspecialchars($_POST['rUsername']);
        $email = $user->Email = htmlspecialchars($_POST['rEmail']);
        $firstname = $user->Firstname = htmlspecialchars($_POST['rFirstname']);
        $lastname = $user->Lastname = htmlspecialchars($_POST['rLastname']);
        $user->Password = htmlspecialchars($_POST['rPassword']);

        $feedback = $user->register();
        if($feedback === "succes"){
            $_SESSION['register'] = "<p class='green'>Succesvol geregistreerd. Login</p>";
            header("Location: login.php");
        }
    }


?><!DOCTYPE html>
<html lang="en">

<head>
    <title>Register - IMDstagram</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.png" sizes="16x16 32x32" type="image/png">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-2.2.3.min.js" defer></script>
    <script src="js/typeahead.min.js" defer></script>
    <script src="js/imd.js" defer></script>
</head>

<body>
    <style>nav{height:70px}</style>
    <?php include_once "includes/nav.php"?>
    <section class="registerForm">
        <a href="index.php" id="mainLogo">Logo</a>
        <form action="" method="POST">
            <?php
            if(isset($feedback)) {
                echo "$feedback";
            }else{
                echo "<p> Nieuw op IMDstagram? Registreer </p>";
            }
            ?>
            <input name="rEmail" class="formInput" type="text" placeholder="Email" maxlength="254" value="<?php if(!empty($email)){ echo "$email";} ?>">
            <input name="rFirstname" class="formInput" type="text" placeholder="Voornaam" maxlength="50" value="<?php if(!empty($firstname)){ echo "$firstname";} ?>">
            <input name="rLastname" class="formInput" type="text" placeholder="Achternaam" maxlength="50" value="<?php if(!empty($lastname)){ echo "$lastname";} ?>">
            <input id="tb_username" name="rUsername" class="formInput" maxlength="15" type="text" maxlength="14" placeholder="Gebruikersnaam" value="<?php if(!empty($username)){ echo "$username";} ?>"><img id="loadingImage" src="images/loading.gif" /><img id="correctImage" src="images/checked-symbol.svg" /><img id="wrongImage" src="images/cancel-mark.svg" />
            <input name="rPassword" id="password" class="formInput" type="text" onfocus="this.type='password'" placeholder="Wachtwoord">
            <input type="submit" id="registerSubmit" name="registerSubmit" value="Registreer">
        </form>
    </section>
     <?php include_once "includes/footer.php"?>
</body>
</html>