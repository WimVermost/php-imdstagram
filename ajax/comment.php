<?php
session_start();
include_once("../classes/Comment.class.php");
$comment = new Comment();
$userId = $_SESSION['id'];
$uploadId = $_POST['uploadid'];
$message = $_POST['message'];

if(!empty($_POST['message'])){
    $comment->Comment = $_POST['message'];
    $comment->Uploadid = $_POST['uploadid'];
    $comment->Userid = $_SESSION['id'];

    try{
        $comment->placeComment();
        $response['status'] = 'success';
    }catch(Exception $e){
        $feedback  = $e->getMessage();
        $response['status'] = 'error';
    }
    header('Content-Type: application/json');
    echo json_encode($response); // status: 'error', message: ...
}

?>