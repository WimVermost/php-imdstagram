<?php
include_once("../classes/Photo.class.php");

if(!empty($_POST['uploadId'])){

    try{
        $delete = new Photo();
        $delete->deletePhoto($_POST['uploadId']);
        $response['status'] = 'deleted';
    }catch(Exception $e){
        $feedback  = $e->getMessage();
        $response['status'] = 'error';
    }
    header('Content-Type: application/json');
    echo json_encode($response);

}

?>