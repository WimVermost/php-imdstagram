<?php
    include_once "../classes/Follow.class.php";
    if(!empty($_POST['followId'])) {

        $follow = new follow();

        $follow->userId = $_POST['userId'];
        $follow->followId = $_POST['followId'];
        $action = $_POST['action'];

        if($action === 'follow'){
            if ($follow->followPerson()) {
                $response['status'] = 'success';
            }else{
                $response['status'] = 'failed';
            }
        }
        if($action === 'unfollow'){

            if ($follow->unfollowPerson()) {
                $response['status'] = 'success';
            }else{
                $response['status'] = 'failed';
            }

        }

        header('Content-type: application/json');
        echo json_encode($response);
    }


?>