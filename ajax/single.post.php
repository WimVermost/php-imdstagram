    <?php session_start();
    include_once("../classes/User.class.php");
    include_once("../classes/Photo.class.php");
    if(isset($_POST['postId'])){
        $p = new Photo();
        $u = new User();
        $p->__SET('Userid', $_SESSION['id']);
        $single = $p->getSinglePhoto($_POST['postId']);
        $single = $single[0];

        $response['username'] = $u->getUsernameWithUserID($single['userId']);
        $response['profileimage'] = $u->getImageWithUserID($single['userId']);
        $response['image'] = $single['photo'];
        $response['timeposted'] = $p->timeConverter($single['timeposted']);
        $response['uploadid'] = $single['uploadId'];
        $response['userId'] = $single['userId'];
        $response['description'] = $single['description'];
        $response['likes'] = $p->getLikes($single['uploadId']);
        $response['comments'] = $p->getComments($single['uploadId']);
        $response['filter'] = $p->getFilter($single['uploadId']);
        $comment_placers = [];
        for ($i = 0; $i < count($response['comments']); ++$i){
            $comment_placer = $u->getUsernameWithUserID($response['comments'][$i][2]);
            array_push($comment_placers, $comment_placer);
        }
        $response['commentplacers'] = $comment_placers;

        if($p->isLiked($single['uploadId'])){
            $response['liked'] = true;
        }else{
            $response['liked'] = false;
        }

        header('Content-type: application/json');
        echo json_encode($response);
    }
?>