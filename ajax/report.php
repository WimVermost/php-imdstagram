<?php

session_start();
include_once("../classes/Photo.class.php");

$userId = $_SESSION['id'];
$uploadId = $_POST['uId'];

if(!empty($_POST['uId'])){

    $report = new photo();

    try{
        $report->reportPhoto($uploadId, $userId);
        $response['status'] = 'success';
    }catch(Exception $e){
        $feedback  = $e->getMessage();
        $response['status'] = 'error';
    }
    header('Content-Type: application/json');
    echo json_encode($response); // status: 'error', message: ...
}

?>