<?php
session_start();

include_once('../classes/Photo.class.php');

if(!empty($_POST['uploadid'])) {
    $photo = new Photo();
    $photo->Userid = $_SESSION['id'];
    $photo->Uploadid = $_POST['uploadid'];
    $userId = $_SESSION['id'];
    $uploadId = $_POST['uploadid'];

    if($photo->isLiked($uploadId)){
        $action = $photo->RemoveLike();
        $likes = $photo->getLikes($uploadId);
        $response['status'] = 'unlike';
        $response['likes']= $likes;
    }else{
        $action = $photo->Like();
        $likes = $photo->getLikes($uploadId);
        $response['status'] = 'like';
        $response['likes']= $likes;
    }
    header('Content-type: application/json');
    echo json_encode($response);

}
?>
