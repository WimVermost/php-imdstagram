<?php
session_start();
include_once("../classes/Photo.class.php");
$Photo = new Photo();
$Photo->Userid = $_SESSION['id'];
$offset = $_POST['offset'];
$count = count($Photo->getFullPhotoList());
$query = $Photo->loadmore($offset);
$allLoaded = "";
if($count-2 <= $offset){
    $allLoaded = "class='last'";
}
$output = "";
foreach($query as $x){
    $username = $x['username'];
    $likes = $Photo->getLikes($x['uploadId']);
    $comm = $Photo->getComments($x['uploadId']);
    $comments = "";
    $location ="";
    $likeImage = "images/heart_line.png";
    $time="";
    foreach($comm as $y){

        $comments .= "<div class='comment'>
                        <p><a class='username' href='profile.php?user=".$y['username']."'>".$y['username']."</a>
                        ".$y['comment']."</p>
                    </div>";

                }
    if($Photo->isLiked($_SESSION['id'],$x['uploadId'])){
        $likeImage = 'images/heart.png';
    }else{
        $likeImage = 'images/heart_line.png';
    }
    if($x['latitude']!= ""){
        $location = "<a href='#' class='locIcon'>Icon</a>".$Photo->get_location($x['latitude'],$x['longitude']);
    }
    $time = $Photo->timeConverter($x['timeposted']);
   $output .= "<article " . $allLoaded . ">
        <div class='title'>
            <a class='user-info' href='profile.php?user=".$username."'>
                <div class='profile_picture_small'><img class='profile_image' src='images/profilepictures/".$x['image']."' alt='Photo of ".$x['username']."'></div>
                <div><h3>".$x['username']."</h3></div>
            </a>
            <div>
                <h4>".$time."</h4>
            </div>
        </div>
        <div class='image " . $x['filter'] . "'>
            <img src='images/useruploads/".$x['photo']."'>
        </div>
         <div class='loc'>".$location."</div>
        <div class='content'>
            <div class='likes' id='like_".$x['uploadId']."'>


                $likes Likes

            </div>
            <div class='comment'>
                <p><a class='username' href='profile.php?user=".$x['username']."'>".$x['username']."</a>
                ".$x['description']."</p>
            </div>

            <!-- loop comments -->
            <div id='commentContainer_".$x['uploadId']."'>

               $comments
            </div>
            <div class='content-footer'>
                <div><img class='like' id='".$x['uploadId']."' src='$likeImage'></div>
                <div class='post'>

                    <input id='".$x['uploadId']."' name='".$x['uploadId']."' type='text' placeholder='Add a comment...' class='placecomment'>

                </div>
                <div class='option' id='report_".$x['uploadId']."'><img src='images/more.png' alt=''></div>
            </div>
        </div>
    </article>";
}
echo $output;