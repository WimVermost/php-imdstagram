$(document).ready(function () {
    var uId;
    var display;

    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $("#tb_username").on("keyup", function(e) {
        //clear feedback
        $("#wrongImage").css('display', 'none');
        $("#correctImage").css('display', 'none');
        var username = $("#tb_username").val();
        if(username !== "") {
            $("#wrongImage").css('display', 'none');
            $("#correctImage").css('display', 'none');
            $("#loadingImage").css('display', 'inline-block');

            //loader show
            delay(function () {

                $.post("ajax/check.username.php", {username: username})
                    .done(function (response) {

                        if (response.status === 'error') {
                            //fout

                            $("#loadingImage").css('display', 'none');
                            $("#wrongImage").css('display', 'inline-block');

                        } else {
                            //correct

                            $("#loadingImage").css('display', 'none');
                            $("#correctImage").css('display', 'inline-block');

                        }
                    });

                e.preventDefault();
            }, 1000);
        }
    });

    if (navigator.geolocation) {
        var timeoutVal = 10 * 1000 * 1000;
        navigator.geolocation.getCurrentPosition(
            setPosition,
            displayError,
            { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 }
        );
    }
    else {
        alert("Geolocation is not supported by this browser");
    }


    $("#locationCheckBox").change(function() {

        if(this.checked) {

        }
    });
    function setPosition(position) {
        $("#lat").val(position.coords.latitude);
        $("#long").val(position.coords.longitude);
    }
    function displayError(error) {
        var errors = {
            1: 'Permission denied',
            2: 'Position unavailable',
            3: 'Request timeout'
        };
        alert("Error: " + errors[error.code]);
    }
    function reload_js(src) {
        $('script[src="' + src + '"]').remove();
        $('<script>').attr('src', src).appendTo('head');
    }

    $(".article").on("click",".like", function(){
        //direct foto veranderen
        var uploadid = $(this).attr("id");
        $.post( "ajax/like.image.php", { uploadid: uploadid})
            .done(function( response ){

                if(response.status === 'like') {
                    $(".index-article #"+uploadid).attr('src', 'images/heart.png');
                    $("#single_post #"+uploadid).attr('src', 'images/heart.png');
                    $("#like_"+uploadid).text(response.likes+" likes");
                }
                else{
                    $(".index-article #"+uploadid).attr('src', 'images/heart_line.png');
                    $("#single_post #"+uploadid).attr('src', 'images/heart_line.png');
                    $("#like_"+uploadid).text(response.likes+" likes");
                }
            });
    });
    $(".btn-loadmore").on("click", function(){
        //direct foto veranderen
        var offsetid = $(this).attr("id").toString();
        offsetid = offsetid.replace("offset_","");
        offsetid = parseInt(offsetid) + 2;
        var newoffset = "offset_" + offsetid;
        this.id = newoffset;
        $.post( "ajax/loadmore.php", { offset: offsetid-2})
            .done(function( response ){
                $(".index-article").append(response);
                reload_js('imd.js');
                if($(".index-article article").last().attr('class') === 'last'){
                    $('.btn-loadmore').css('display', 'none');
                }
            });

    });

    var open = false;
    $('.createPostHeader').click(function(){
        if(open){
            $('#imageform').slideUp();
            open = false;
        }
        else{
            $('#imageform').slideDown();
            open = true;
        }
    });

    // index -- report frame
    $('#btn-report-cancel').click(function(){
        $('.modal').css("display", "none");
    });
    $('.article').on('click','.option',function(){
        uId = $(this).attr("id").toString();
        uId = uId.replace("report_", "");
        $('.modal').css("display", "block");
    });

    // Code that open and closes 'change profile image' screen.
    $('.profile_image_button').click(function() {
        $('#change_profilephoto').css("display", "block");
    });
    $('#control_panel_profilephoto a').click(function() {
        $('#change_profilephoto').css("display", "none");
    });

    // Code that open and closes 'edit_profile' screen.
    $('.edit_profile_button').click(function() {
        $('#edit_profile').css("display", "block");
    });
    $('#edit_profile a').click(function() {
        $('#edit_profile').css("display", "none");
    });
    $('.exit').click(function() {
        $('#change_profilephoto').css("display", "none");
        $('#edit_profile').css("display", "none");
        $('#single_post').css("display", "none");
        $("#single_post div.commentContainer").empty();
        display = false;
    });

    // On edit profile, switch between edit_profile and edit_password
    $('#edit_account').click(function() {
        $('#edit_password_form').slideUp();
        $('#edit_profile_form').slideDown();
        $('#edit_password').css("cursor", "pointer");
        $('#edit_account').css("cursor", "auto");
        $('#edit_password').css("color", "#6C6C6C");
        $('#edit_account').css("color", "black");
    });
    $('#edit_password').click(function() {
        $('#edit_profile_form').slideUp();
        $('#edit_password_form').slideDown();
        $('#edit_account').css("cursor", "pointer");
        $('#edit_password').css("cursor", "auto");
        $('#edit_password').css("color", "black");
        $('#edit_account').css("color", "#6C6C6C");
    });

    // Submits 'change profile image' instant when file selected.
    $("#upload_foto").change(function() {
        $("#imageform").submit();
    });

    // Sticky footer
    $(function(){
        var footer = $("#footer");
        var pos = footer.position();
        var height = $(window).height();
        height = height - pos.top;
        height = height - footer.height();
        $('footer').css('opacity','1');
        if (height > 0) {
            footer.css({
                'margin-top': height + 'px'
            });
        }
    });
    $( window ).resize(function() {
        var footer = $("#footer");
        var pos = footer.position();
        var height = $(window).height();
        height = height - pos.top;
        height = height - footer.height();
        if (height > 0) {
            footer.css({
                'margin-top': height + 'px'
            });
        }
    });

    // escapeHTML zorgt ervoor dat je geen tags kan ingeven, anders is heel de pagina omzeep
    function escapeHtml(comment) {
        return comment
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
    }
    function commentConvert(comment){
        var message = comment;
        var newString = "";
        var newMessage = "";
        var messageSplit = message.split(" ");
        messageSplit.forEach(function(entry) {
            if(entry.indexOf("@") == 0){
                var name = entry.replace("@","");
                newString = "<a href='profile.php?user="+name+"'>"+entry+"</a>";
            }else if(entry.indexOf("#") == 0){
                var name = entry.replace("#","");
                newString = "<a href='tag.php?tag="+name+"'>"+entry+"</a>";
            }else{
                newString = entry;
            }
            newMessage = newMessage + newString + " ";
        }, this);
        return newMessage;
    }
    $(".article").on('keypress','.placecomment',function (e) {
        if (e.which == 13) {
            var message = String($(this).val())
           if(!jQuery.trim( message ).length == 0){
                //checks for spaces
                message = escapeHtml(message);
                //checks for tags/xss
                var uploadId = String($(this).attr("id").toString());
                var name = $("#activeUser").val();
                $.post( "ajax/comment.php", { uploadid: uploadId, message: message})
                    .done(function( response ){
                        if(response.status === 'success') {
                            var newMessage = commentConvert(message);
                            var comment = "<div class='comment'><p><a class='username' href='/profile.php?user="+name+"'>"+name+"</a>"+newMessage+"</p></div>";
                            $("#commentContainer_"+uploadId).append(comment);;
                            $(".placecomment").val('');
                        }
                  });
            }
        }
    });
    if($('.follow_button.normal').hasClass('following')) {
        var followed = true;
    }else{
        var followed = false;
    }

    $(".follow_button.normal").on("click", function(){
        if(!followed){

        $.post( "ajax/follow.php", { userId: userId, followId: followId, action: 'follow'})
            .done(function( response ){

                if(response.status === 'success') {
                    $(".follow_button").css('color','green');
                    $(".follow_button").css('border-color','green');
                    $(".follow_button").text('Following');
                    followed = true;
                }
            });
        }
        else{
            location.reload();

            $.post( "ajax/follow.php", { userId: userId, followId: followId, action: 'unfollow'})
                .done(function( response ){

                    if(response.status === 'success') {
                        $(".follow_button").css('color','#125688');
                        $(".follow_button").css('border-color','#125688');
                        $(".follow_button").text('Follow');
                        followed = false;
                    }
                });
        }
    });

    $("#sendReport").on("click", function(){

        $.post( "ajax/report.php", { uId: uId})
            .done(function( response ){
                if(response.status === 'success') {
                    $(".photo-reported").css("display","flex");
                    $('.modal').css("display", "none");
                    setTimeout(function() {
                        $(".photo-reported").fadeOut( "slow" );

                    }, 5000);
                }
            });
    });
    $(".remove_img").on("click", function(){
        var uploadid = $(this).attr("data-id");
        $.post( "ajax/delete.image.php", { uploadId: uploadid})
            .done(function( response ){

                if(response.status === 'deleted') {
                 $("#"+uploadid).hide();
                }

            });
    });
    // autofill user on search
    $('input.typeahead').typeahead({
        name: 'typeahead',
        remote: 'ajax/search.user.php?key=%QUERY',
        limit: 10
    });
    $(".searchBarNav").keypress(function(event) {
        if (event.which == 13) {
            if($(this).val().substring(0, 1) === '#'){
                var tag = $(".searchBarNav").val();
                tag = tag.replace("#", "");
                window.location.href = "tag.php?tag=" + tag;
            }else{
                var name = $(".searchBarNav").val();
                window.location.href = "profile.php?user=" + name;
            }
        }
    });

    //hover option efect
    $(".option")
        .mouseover(function() {
            $(".option img").attr('src', 'images/more_hover.png');
        })
        .mouseout(function() {
            $(".option img").attr('src', 'images/more.png');
        });

    //show single post using ajax
    $( ".profile-user-img-block img" ).click(function() {
        var postId = $(this).parent().attr('id');
        $.post("ajax/single.post.php", {postId: postId})
        .done(function( response ){
            var profile_link = "profile.php?user="+response.username;
            $('#single_post .user-info').attr("href", profile_link);
            var profile_image = "images/profilepictures/" + response.profileimage;
            $('#single_post .profile_image').attr("src", profile_image);
            $('#single_post .profile_image').attr("alt", "Photo of " + response.username);
            $('#h3-username').html(response.username);
            var photo = 'images/useruploads/' + response.image;
            $('#single_post .image img').attr("src", photo);
            $('#single_post .like').attr("id", response.uploadid);
            if(response.liked === true){
                $('#single_post .like').attr("src", "images/heart.png");
            }else{
                $('#single_post .like').attr("src", "images/heart_line.png");
            };
            $('#single_post .likes').html("<span class='bolder'>"+response.likes+ "</span>" + " likes");
            var likes_id = "like_" + response.uploadid;
            $('#single_post .likes').attr("id", likes_id);
            $('#single_post .content h4').html(response.timeposted);
            $('#single_post .post_description a').attr("href", profile_link);
            $('#single_post .post_description a').html(response.username);
            $('#single_post .post_description .mess').html(response.description);
            $('#single_post .commentContainer').attr("id", "commentContainer_" + response.uploadid);
            $('#single_post #comment-box input').attr("id", response.uploadid);
            $('#single_post #comment-box input').attr("name", response.uploadid);
            $('#single_post .option').attr("id", "report_" + response.uploadid);
            if(response.filter !== ""){
                $filter = "image " + response.filter;
                $('.image').attr("class", $filter);
            }else{
                $('.image').removeClass().addClass("image");
            }
            for (var i in response.comments) {
                var comment_username = response.commentplacers[i];
                var comment = "<div class='comment'><p class='username'><a href='/profile.php?user=" + comment_username + "'>" + comment_username + "</a>&nbsp;&nbsp;</p><p class='mess'>" + response.comments[i][3] + "</p></div>";
                $("#single_post div.commentContainer").append(comment);
            }

            if($(window).width() > 1000) {
                var g = 450 - 88;
                $('#reaction-box').css('height', g + 'px');
            }

            $('#single_post').css("display", "block");
    // some tricks to make it work/look good
            if($(window).width() < 1000){
                $('html, body').css({
                    'overflow': 'hidden',
                    'height': '100%'
                });
                display = true;
                var h = $( window ).height() - ($(".title").height() + $(".image").height() + $("#head-box").height() + $("#comment-box").height() + 50);
                $('#reaction-box').css('height', h+'px');
            }
        });
    });

    if ($(window).width() > 600){
        var i = 1;
    }else{
        var i = 0;
    }

    $(window).resize(function() {
        var windowsize = $(window).width();
        if (windowsize > 600 && i === 0) {
            i = 1;
            location.reload();
        } else if (windowsize <= 600 && i == 1) {
            i = 0;
            location.reload();
        }
        if($(window).width() < 1000 && display === true) {
            var h = $(window).height() - ($(".title").height() + $(".image").height() + $("#head-box").height() + $("#comment-box").height() + 50);
            $('#reaction-box').css('height', h + 'px');
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }else{
            $('#reaction-box').css('height', '100px');
        }
    });
    $( ".x" ).click(function() {
        $('html, body').css({
            'overflow': 'auto',
            'height': 'auto'
        });
    });

    // show or hide follow requests
    var requests = false;
    $( "#follower_request" ).click(function() {
        if(requests === false) {
            $("#follow_inbox").slideDown(200);
            requests = true;
        }else{
            $("#follow_inbox").slideUp(200);
            requests = false;
        }
    });
});

// previeuw image
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#pre-vieuw').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
$("#photoimg").change(function(){
    readURL(this);
    $('#pre-vieuw').css('display', 'block');
});

// previeuw filter
$('#filter_select').on('change', function() {
    $('#filter-box').removeClass();
    $('#filter-box').toggleClass( this.value );
});