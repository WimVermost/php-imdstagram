<?php
    include_once('classes/User.class.php');
    include_once('classes/Photo.class.php');
    include_once('classes/Follow.class.php');
    session_start();

    if(!isset($_SESSION['id'])){
        header('location: login.php');
    }

    if(!isset($_GET['user'])){
        header('location: index.php');
    }

    // Checking some things.
    $profileUsername = $_GET['user'];
    $user = new User();
    if($user->userExists("$profileUsername")){
        $user->getUserByUsername("$profileUsername");
        $username = $user->__GET("Username");
        $isActiveUser = $username === $_SESSION["username"];

        // Preparing to display user.
        $userId = $user->getUserIdWithUsername($profileUsername);

        $firstname = $user->__GET("Firstname");
        $lastname = $user->__GET("Lastname");
        $email = $user->__GET("Email");
        $posts = $user->postsCount($userId);
        $followers = $user->followersCount($userId);
        $following = $user->followingCount($userId);
        
        if(null !== $user->__GET("Biography")){
            $biography = $user->__GET("Biography");
        }
        if(null !== $user->__GET("Website")){
            $website = $user->__GET("Website");
        }
        if(null !== $user->__GET("Image")){
            $image = $user->__GET("Image");
        }
        if(null !== $user->__GET("Sex")){
            $sex = $user->__GET("Sex");
        }
        if(null !== $user->__GET("Private")){
            $private = $user->__GET("Private");
        }
    }else{
        header('location: index.php');
    };

    // change profilephoto
    $form_message = "";
    if(isset($_FILES['profileimg'])){
        $path = "images/profilepictures/";
        $valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
        $name = $_FILES['profileimg']['name'];
        $size = $_FILES['profileimg']['size'];
        if(strlen($name)){
            list($txt, $ext) = explode(".", $name);
            if(in_array($ext,$valid_formats)) {
                if($size > 10240) {
                    $imagename = time() . $_SESSION['id'] . "." . $ext;
                    $tmp = $_FILES['profileimg']['tmp_name'];
                    move_uploaded_file($tmp, $path . $imagename);
                    if (isset($image)) {
                        unlink("images/profilepictures/$image");
                    }
                    $userId = $_SESSION['id'];
                    $user->__SET("Image", "$imagename");
                    $user->updateSQL("Image", $user->__GET("Image"), $_SESSION['id']);
                    $message = NULL;
                    header("Refresh:0");
                }else{
                    $message = "Profiel foto mag niet groter dan 1mb zijn.";
                }
            }else{
                $message = "Dit bestand type word niet ondersteund.";
            }
        }
    }

    // edit profile
    if(isset($_POST['profile_submit'])) {
        if (!empty($_POST['profile_firstname']) && !empty($_POST['profile_lastname']) && !empty($_POST['profile_email']) && !empty($_POST['profile_username'])) {
            $u = new User();
            $u->getUserByUsername($_SESSION['username']);
            $active_user_email = $u->__GET('Email');
            $form_message = $u->editUserForm($_SESSION['username'], $active_user_email);
            $url = "/IMDstagram/profile.php?user=" . $_SESSION['username'];
            if($form_message === false) {
                header("Refresh:1;");
            }
        } else {
            $form_message = "Gelieve alle vereiste velden in te vullen.";
        }
    }

    // edit password
    if(isset($_POST['password_submit'])) {
        if (!empty($_POST['profile_current_password']) && !empty($_POST['profile_new_password']) && !empty($_POST['profile_repeat_new_password'])){
            $u = new User();
            $u->getUserByUsername($_SESSION['username']);
            $active_user_email = $u->__GET('Email');
            $new_password = htmlspecialchars($_POST['profile_new_password']);
            $new_password_again = htmlspecialchars($_POST['profile_repeat_new_password']);
            if($u->correctPassword($_SESSION['username'], htmlspecialchars($_POST['profile_current_password']))){
                if($new_password === $new_password_again) {
                    if (strlen($new_password) < 5) {
                        $password_form_message = "Het paswoord moet langer dan 5 karakters zijn.";
                    } else if (!preg_match("#[0-9]+#", $new_password)) {
                        $password_form_message = "Het passwoord moet 1 cijfer bevatten.";
                    } else if (!preg_match("#[a-z]+#", $new_password)) {
                        $password_form_message = "Het passwoord moet 1 letter hebben.";
                    } else {
                        $new_password = password_hash($new_password, PASSWORD_DEFAULT);
                        $u->updateSQL('Password',$new_password,$_SESSION['id']);
                        $form_message = false;
                    }
                }else{
                    $password_form_message = "Niet twee keer hetzelfde wachtwoord.";
                }
            }else{
                $password_form_message = "Fout wachtwoord.";
            }
        }else{
            $password_form_message = "Gelieve alle vereiste velden in te vullen.";
        }
    }

?><!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $_GET['user'] ?> - IMDstagram</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.png" sizes="16x16 32x32" type="image/png">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/cssgram.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-2.2.3.min.js" defer></script>
    <script src="js/typeahead.min.js" defer></script>
    <script src="js/imd.js" defer></script>
</head>
<body>
<input type="hidden" id="activeUser" value="<?php echo $_SESSION['username']; ?>">
    <?php include_once "includes/nav.php"; ?>
    <?php
        $followId = $user->getUserIdWithUsername($_GET['user']);
        $userId = $_SESSION['id'];
        echo "<script>";
        echo 'var followId = ' . json_encode($followId) . ';';
        echo 'var userId = ' . json_encode($userId) . ';';
        echo "</script>";
    ?>
    <article class="profile_page">
        <header class="profile_banner">

            <?php if(isset($message)): ?>
            <div id="profileMessage">
                <h1><?php echo "$message"  ?></h1>
            </div>
            <?php endif; ?>

            <?php if($form_message === false) {
                echo "<div id='profileMessage' class='green'><h1>Succes.</h1></div>";
            }else if(!empty($form_message) || isset($_POST['reset'])){
                echo "<style>#edit_profile{display: block}</style>";
            }else if(!empty($password_form_message)){
                echo "<style>#edit_profile{display: block}#edit_profile_form{display: none}#edit_password_form{display: block}</style>";
            } ?>

            <div class="profile">
                <div class="profile_picture">
                    <?php
                    if($isActiveUser) {
                        echo "<button class='profile_image_button' >Verander profielfoto</button >";
                    }
                    if(!empty($image)) {
                        echo "<img class='profile_image' src='images/profilepictures/" . "$image" . "' alt='profile_image'>";
                    }else{
                        echo "<img class='profile_image' src='images/profilepictures/placeholder.jpg' alt='profile_image'>";
                    } ?>
                </div>
                <div class="profile_information">
                    <div class='edit_profile'>
                        <?php
                        echo "<h1>". "$username" ."</h1>";
                        if($isActiveUser){
                            echo "<button class='edit_profile_button'>Bewerk profiel</button>";
                        }else{
                            $follow = new Follow();
                            $follow->userId = $userId;
                            $follow->followId = $followId;
                            if($follow->isFollowing()){
                                $follow_status = 'following';
                                echo "<button class='follow_button normal " . $follow_status . "'>" . $follow_status . "</button>";
                            }else{
                                $isPrivate = User::isPrivate($followId);
                                if(!$isPrivate) {
                                    $follow_status = 'follow';
                                    echo "<button class='follow_button normal " . $follow_status . "'>" . $follow_status . "</button>";
                                }else{
                                    if(!Follow::hasSubmitedRequest($_SESSION['id'], $followId)) {
                                        $follow_status = 'follow request';
                                        echo "<form method='post' id='requestForm'>";
                                        echo "<button form='requestForm' name='requestForm' type='submit' class='follow_button request'>" . $follow_status . "</button>";
                                        echo "</form>";
                                    }else{
                                        $follow_status = 'Request verzonden';
                                        echo "<button class='follow_button " . $follow_status . "'>" . $follow_status . "</button>";
                                    }

                                    if(isset($_POST['requestForm'])){
                                        $t = Follow::setFollowRequest($_SESSION['id'], $followId);
                                        //header("Refresh:0");
                                    }
                                }
                            }
                        } ?>
                    </div>
                    <div class="profile_details">
                        <h2> <?php echo "$firstname" . " " . "$lastname" ?> </h2>
                        <?php
                        if(isset($biography)) {
                            echo "<h2>" . "$biography" . "</h2>";
                        }
                        if(isset($website)){
                            echo "<a target=\"_blank\" href='http://" . "$website" . "'>" . "$website" . "</a>";
                        } ?>
                    </div>
                </div>
            </div>
            <div class='edit_profile_small'>
                <?php
                if($isActiveUser){
                    echo "<button class='edit_profile_button'>Bewerk profiel</button>";
                }else {
                    $follow = new Follow();
                    $follow->userId = $userId;
                    $follow->followId = $followId;
                    if ($follow->isFollowing()) {
                        echo "<button class='follow_button'>Follow</button>";
                    } else{
                        $isPrivate = User::isPrivate($followId);
                        if(!$isPrivate) {
                            $follow_status = 'follow';
                            echo "<button class='follow_button normal " . $follow_status . "'>" . $follow_status . "</button>";
                        }else{
                            if(!Follow::hasSubmitedRequest($_SESSION['id'], $followId)) {
                                $follow_status = 'follow request';
                                echo "<form method='post' id='requestForm'>";
                                echo "<button form='requestForm' name='requestForm' type='submit' class='follow_button request'>" . $follow_status . "</button>";
                                echo "</form>";
                            }else{
                                $follow_status = 'Request verzonden';
                                echo "<button class='follow_button " . $follow_status . "'>" . $follow_status . "</button>";
                            }

                            if(isset($_POST['requestForm'])){
                                $t = Follow::setFollowRequest($_SESSION['id'], $followId);
                                header("Refresh:0");
                            }
                        }
                    }
                }
                 ?>
            </div>
            <ul class="user_numbers">
                <li class="posts">
                    <p><span><?php echo "$posts" ?></span> posts</p>
                </li>
                <li class="followers">
                    <p><span><?php echo "$followers" ?></span> followers</p>
                </li>
                <li class="following">
                    <p><span><?php echo "$following" ?></span> following</p>
                </li>
            </ul>
        </header>
        <?php
        $follow = new Follow();
        $follow->__SET('userId', $_SESSION['id']);
        $follow->__SET('followId', $user->getUserIdWithUsername($profileUsername));
        $follow = $follow->isFollowing();
        if(!User::isPrivate($followId) || ($follow && User::isPrivate($followId))): ?>
        <div class="profile_images">
            <?php
            $Photo = new Photo();
            $Loads = $Photo->getUserPhotoList($user->getUserIdWithUsername($profileUsername));
            foreach($Loads as $x) { ?>
                <?php $filter = $x['filter']; ?>
                <div class="profile-user-img-block<?php if(!empty($filter)){echo" $filter";} ?>" <?php echo "id='" . $x['uploadId'] . "'"?>>
                    <?php if($isActiveUser){
                        echo "<button class='remove_img' data-id='". $x['uploadId'] ."'>Foto verwijderen</button>";
                    }; ?>
                    <img src="images/useruploads/<?php echo $x['photo'];?>" alt="" >
                </div>
            <?php } ?>
        </div>
        <?php endif; if(!$follow && User::isPrivate($followId)): ?>
            <h3 class="privateAccount">Dit is een privé account, stuur een follow request.</h3>
        <?php endif; ?>
    </article>
    <div class="modal report">
        <div class="modal-content">
            <p>Reason:</p>
            <textarea placeholder="Optional..."></textarea>
            <div class="modal-content-btn">
                <a href="#" id="sendReport">Report inappropriate</a>
                <a href="#" id="btn-report-cancel">Cancel</a>
            </div>
        </div>
    </div>
    <?php include_once "includes/footer.php"?>

    <!-- Pop-up screens as include to keep everything easy to overview. -->
    <?php include_once "includes/edit_profile.php"?>
    <?php include_once "includes/single_post.php"?>
</body>
</html>