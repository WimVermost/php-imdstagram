<?php

// dir waar foto's komen
$path = "images/useruploads/";
// formats die toegelaten zijn
$valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");

if (isset($_POST['createPost'])) {

    $description = $_POST['userdescription'];
    if (strlen(trim($description)) == 0){
         $message = "Your description can not be empty.";
    }else{
        $name = $_FILES['photoimg']['name'];
        $size = $_FILES['photoimg']['size'];
        if (strlen($name)) {

            $array = explode(".", $name);
            $size = count($array) -1;
            $ext = $array[$size];

            if (in_array($ext, $valid_formats)) {
                $imagename = time() . $_SESSION['id'];
                $tmp = $_FILES['photoimg']['tmp_name'];
                move_uploaded_file($tmp, $path . $imagename);

                $photo = new Photo();
                $photo->Photo = $imagename;
                $photo->Filter = htmlspecialchars($_POST['filter']);
                $photo->Description = htmlspecialchars($_POST['userdescription']);
                $photo->Userid = $_SESSION['id'];
                $photo->Longitude = htmlspecialchars($_POST['long']);
                $photo->Latitude = htmlspecialchars($_POST['lat']);
                if ($photo->create()) {
                    header('location: index.php?upload=success');
                    $message = "Photo was successfully uploaded.";
                } else {
                    $message = "Photo upload failed.";
                }

            }
        }
    }
}

if (isset($_GET['upload'])) {
    $message = "Photo succesfully uploaded!";
}

?>