<nav>
    <?php include_once "classes/Follow.class.php"; ?>
    <div id="navWrapper">
        <a href="index.php" id="navLogo"></a>
        <?php
        if(isset($_SESSION['id'])) {
            $isPrivate = User::isPrivate($_SESSION['id']);
        }
        if(isset($_SESSION['loggedin'])){
            $activeUser = $_SESSION['username'];
            $requests = Follow::countFollowRequests($_SESSION['id']);
            echo "
                <form id='searchUser' name='search' action='' method='post'>
                    <input type='text' name='typeahead' class='typeahead tt-query searchBarNav' autocomplete='off' spellcheck='false' placeholder='Zoek op # of gebruiker'>
                </form>";
            if(isset($_GET['user'])){
                $profile = $_GET['user'];
                if($profile === $activeUser){
                    if($isPrivate) {
                        echo "<div id='navName'><button id='follower_request' href='#' name='follow requests'><span class='request_amount'>".$requests."</span></button><a href='logout.php' name='" . $activeUser . "'>Log out</a></div>";
                    }else{
                        echo "<div id='navName'><a href='logout.php' name='" . $activeUser . "'>Log out</a></div>";
                    }
                }else{
                    if($isPrivate) {
                        echo "<div id='navName'><button id='follower_request' href='#' name='follow requests'><span class='request_amount'>$requests</span></button><a href='profile.php?user=" . "$activeUser" . "' name='" . $activeUser . "'>" . "$activeUser" . "</a></div>";
                    }else{
                        echo "<div id='navName'><a href='profile.php?user=" . "$activeUser" . "' name='" . $activeUser . "'>" . "$activeUser" . "</a></div>";
                    }
                }
            }else{
                if($isPrivate) {
                    echo "<div id='navName'><button id='follower_request' href='#' name='follow requests'><span class='request_amount'>$requests </span></button><a href='profile.php?user=" . "$activeUser" . "' name='" . $activeUser . "'>" . "$activeUser" . "</a></div>";
                }else{
                    echo "<div id='navName'><a href='profile.php?user=" . "$activeUser" . "' name='" . $activeUser . "'>" . "$activeUser" . "</a></div>";
                }
        }}?>
    </div>
    <?php
    if(isset($_SESSION['id'])) {
        $isPrivate = User::isPrivate($_SESSION['id']);
        $u = new User();
        if($isPrivate): ?>
        <div id="follow_inbox">
            <div>
                <ul>
                    <h1>Follow Requests</h1>
                    <?php
                    $reqs = Follow::getFollowRequests($_SESSION['id']);
                    foreach($reqs as $x) {
                    if(isset($x)){
                        $userReqId = $x['userId'];
                        $userReq = $u->getUsernameWithUserID($userReqId);
                        ?>
                        <li>
                            <p><?php echo"$userReq" ?></p>
                            <form action="" method="post">
                                <button type="submit" name="accept" name="ha" class="accept">Aanvaarden</button>
                            </form>
                            <form action="" method="post">
                                <button type="submit" name="refuse" class="refuse">Negeren</button>
                            </form>
                        </li>
                    <?php }};

                    if(isset($_POST['accept'])){
                        Follow::acceptRequest($x['userId'], $_SESSION['id']);
                        Follow::removeRequest($x['userId'], $_SESSION['id']);
                        header("Refresh:0");
                    }

                    if(isset($_POST['refuse'])){
                        Follow::removeRequest($x['userId'], $_SESSION['id']);
                        header("Refresh:0");
                    }

                    ?>
                </ul>
            </div>
        </div>
    <?php endif; }?>
</nav>