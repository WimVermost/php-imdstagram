<div id="single_post">
    <article class="article">
        <a href="javascript:void(0)" class="exit x">&#10006;</a>
        <div class="main_info">
            <div class="title">
                <a class="user-info" href="">
                    <div class="profile_picture_small"><img class="profile_image" src="" alt=""></div>
                    <div><h3 id="h3-username"></h3></div>
                </a>
            </div>
            <div class="image">
                <img src="" alt="">
            </div>
        </div>

        <div class="content">
            <div id="head-box">
                <div id="like-box">
                    <div class="likes"></div>
                </div>
                <h4></h4>
            </div>
            <div id="reaction-box">
                <div class="comment post_description">
                    <p class="username"><a href=""></a>&nbsp;&nbsp;</p>
                    <p class="mess"></p>
                </div>
                <!-- loop comments -->
                <div class="commentContainer">

                </div>
            </div>
            <div id="comment-box">
                <div id="comment-input">
                    <div class="btn-like"><img class="like" src="" alt=""></div>
                    <div class="post">
                        <input type="text" placeholder="Add a comment..." class="placecomment">
                    </div>
                </div>
                <div class="option"><img src="images/more.png" alt=""></div>
            </div>
        </div>
    </article>
</div>