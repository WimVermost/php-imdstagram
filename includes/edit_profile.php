<div id="change_profilephoto">
    <a href="javascript:void(0)" class="exit">&#10006;</a>
    <div id="control_panel_profilephoto">
        <h1>Verander je profiel foto</h1>
        <form method="POST" action='' id="imageform" enctype="multipart/form-data">
            <input type="file" name="profileimg" id="upload_foto" >
            <h2 id="upload_foto_cover">Upload Foto</h2>
        </form>
        <a href="javascript:void(0)" class=".exit">Annuleer</a>
    </div>
</div>

<div id="edit_profile">
    <a href="javascript:void(0)" class="exit">&#10006;</a>
    <div id="control_panel_profile">
        <h1 id="edit_account">Bewerk profiel</h1>
        <form id="edit_profile_form" action="" method="POST">
            <ul>
                <?php if(isset($form_message) && isset($_POST['profile_submit']) && $form_message != false){
                    echo "<li><h2 class='error_edit_profile'>" . "$form_message" . "</h2></li>";
                } ?>
                <li>
                    <p>Voornaam*</p>
                    <input type="text" name="profile_firstname" maxlength="50" value="<?php if(isset($profile_firstname)){ echo "$profile_firstname";}else if(isset($firstname)){echo "$firstname";} ?>">
                </li>
                <li>
                    <p>Achternaam*</p>
                    <input type="text" name="profile_lastname" maxlength="50" value="<?php if(isset($profile_lastname)){ echo "$profile_lastname";}else if(isset($lastname)){echo "$lastname";} ?>">
                </li>
                <li>
                    <p>Email*</p>
                    <input type="email" name="profile_email" maxlength="254" value="<?php if(isset($profile_email)){ echo "$profile_email";}else if(isset($email)){echo "$email";} ?>">
                </li>
                <li>
                    <p>Gebruikernaam*</p>
                    <input type="text" name="profile_username" maxlength="14" value="<?php if(isset($profile_username)){ echo "$profile_username";}else if(isset($username)){echo "$username";} ?>">
                </li>
                <li>
                    <p>Biografie</p>
                    <textarea name="profile_bio" maxlength="150"><?php if(isset($profile_bio)){ echo "$profile_bio";}else if(isset($biography)){echo "$biography";} ?></textarea>
                </li>
                <li>
                    <p>Geslacht</p>
                    <select name="profile_sex">
                        <option value="NULL">----</option>
                        <option value="male" <?php if(isset($sex)){if($sex=='male'){echo "selected='selected'";}} ?>>Man</option>
                        <option value="female" <?php if(isset($sex)){if($sex=='female'){echo "selected='selected'";}} ?>>Vrouw</option>
                    </select>
                </li>
                <li>
                    <p>Website</p>
                    <input type="text" name="profile_website" maxlength="254" value="<?php if(isset($profile_website)){ echo "$profile_website";}else if(isset($website)){echo "$website";} ?>">
                </li>
                <li>
                    <p>Privé account</p>
                    <input type="checkbox" name="profile_private" id="private_setting" <?php if($private){echo "checked='checked'";} ?>>
                </li>
                <li>
                    <button type="submit" name="profile_submit">Veranderingen opslaan</button>
                </li>
            </ul>
        </form>
        <h1 id="edit_password">Verander wachtwoord</h1>
        <form id="edit_password_form" action="" method="POST">
            <ul>
                <?php if(isset($password_form_message)){
                    echo "<li><h2 class='error_edit_profile'>" . "$password_form_message" . "</h2></li>";
                } ?>
                <li>
                    <p>Oud wachtwoord</p>
                    <input type="password" name="profile_current_password" >
                </li>
                <li>
                    <p>Nieuw wachtwoord</p>
                    <input type="password" name="profile_new_password" >
                </li>
                <li>
                    <p>Herhaal wachtwoord</p>
                    <input type="password" name="profile_repeat_new_password" >
                </li>
                <li>
                    <button type="submit" name="password_submit">Verander wachtwoord</button>
                </li>
            </ul>
        </form>
        <a href="javascript:void(0)" class=".exit">Annuleer</a>
    </div>
</div>