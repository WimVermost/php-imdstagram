<?php
include_once "Db.class.php";
class Photo
{
    private $m_sPhoto;
    private $m_sFilter;
    private $m_sDescription;
    private $m_userid;
    private $m_reports;
    private $m_uploadid;
    private $m_longitude;
    private $m_latitude;

    function __SET($p_sProperty, $p_vValue)
    {
        switch ($p_sProperty) {
            case "Photo":
                $this->m_sPhoto = $p_vValue;
                break;
            case "Filter":
                $this->m_sFilter = $p_vValue;
                break;
            case "Description":
                $this->m_sDescription = $p_vValue;
                break;
            case "Userid":
                $this->m_userid = $p_vValue;
                break;
            case "Uploadid":
                $this->m_uploadid = $p_vValue;
                break;
            case "Reports":
                $this->m_sDescription = $p_vValue;
                break;
            case "Longitude":
                $this->m_longitude = $p_vValue;
                break;
            case "Latitude":
                $this->m_latitude = $p_vValue;
                break;
        }
    }
    function __GET($p_sProperty)
    {
        switch ($p_sProperty){
            case "Photo":
                return $this->m_sPhoto;
                break;
            case "Filter":
                return $this->m_sFilter;
                break;
            case "Description":
                return $this->m_sDescription;
                break;
            case "Userid":
                return $this->m_userid;
                break;
            case "Uploadid":
                return $this->m_uploadid;
                break;
            case "Reports":
                return $this->m_reports;
                break;
            case "Longitude":
                return $this->m_longitude;
                break;
            case "Latitude":
                return $this->m_latitude;
                break;
        }
    }

    function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    function endsWith($haystack, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }

    function covertComment($p_comment){
        $commentSplit = explode(" ",$p_comment);

        $newString = "";
        foreach($commentSplit as $value){
            if($this->startsWith($value, "#")){
                $name = str_replace("#","",$value);
                $newValue = "<a href='tag.php?tag=$name'>$value</a>";
            }elseif($this->startsWith($value,"@")){
                $name = str_replace("@","",$value);
                $newValue = "<a href='profile.php?user=$name'>$value</a>";
            }else{
                $newValue = $value;
            }

            $newString .= $newValue." ";
        }
        return $newString;
    }

    function get_location ($lat, $long) {
        $key = "AIzaSyA-4ZnK74Tau3lW8qARsUjotgISPJ34x20";
        $get_API = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$long."&key=".$key;
        $jsonfile = file_get_contents($get_API);
        $jsonarray = json_decode($jsonfile);

        if (isset($jsonarray->results[1]->address_components[1]->long_name)) {
           return $jsonarray->results[1]->address_components[0]->long_name . ", " . $jsonarray->results[1]->address_components[3]->long_name;
        }
        else {
            return('');
        }

    }

    public function create(){

        $newDescription = $this->covertComment($this->m_sDescription);
        $conn = Db::getInstance();
        $statement = $conn->prepare("INSERT INTO `tblUploads`(`userid`,`photo`, `filter`, `description`, `longitude`, `latitude`)
                                                      VALUES (:userid, :photo, :filter, :description, :longitude, :latitude)");
        $statement->bindValue(":userid", $this->m_userid);
        $statement->bindValue(":photo", $this->m_sPhoto);
        $statement->bindValue(":filter", $this->m_sFilter);
        $statement->bindValue(":description", $newDescription);
        $statement->bindValue(":longitude", $this->m_longitude);
        $statement->bindValue(":latitude", $this->m_latitude);
        return $statement->execute();
    }

    public function getPhotoList(){

        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT FollowID FROM tblFollowers WHERE userId=:userID");
        $stmt->bindparam(":userID", $this->m_userid);
        $stmt->execute();
        $PhotoRow=$stmt->fetchAll();
        $followers= array();

        foreach($PhotoRow as $row){
            array_push($followers, $row['FollowID']);
        }
        $in = str_repeat('?,', count($followers)-1) . '?';

        $sql = "SELECT * FROM tblUploads INNER JOIN tblUsers ON tblUploads.userId=tblUsers.userID WHERE tblUploads.userID IN ($in) ORDER BY uploadId DESC LIMIT 2 ";
        $stmt = $conn->prepare($sql);

        $stmt->execute($followers);
        $UploadRow = $stmt->fetchAll();
        return $UploadRow;

    }

    public function getFullPhotoList(){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT FollowID FROM tblFollowers WHERE userId=:userID");
        $stmt->bindparam(":userID", $this->m_userid);
        $stmt->execute();
        $PhotoRow=$stmt->fetchAll();
        $followers= array();

        foreach($PhotoRow as $row){
            array_push($followers, $row['FollowID']);
        }
        $in = str_repeat('?,', count($followers)-1) . '?';

        $sql = "SELECT * FROM tblUploads INNER JOIN tblUsers ON tblUploads.userId=tblUsers.userID WHERE tblUploads.userID IN ($in) ORDER BY uploadId";
        $stmt = $conn->prepare($sql);

        $stmt->execute($followers);
        $UploadRow = $stmt->fetchAll();
        return $UploadRow;
    }

    public function loadmore($p_limit){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT FollowID FROM tblFollowers WHERE userId=:userID");
        $stmt->bindparam(":userID", $this->m_userid);
        $stmt->execute();
        $PhotoRow=$stmt->fetchAll();
        $followers= array();

        foreach($PhotoRow as $row){
            array_push($followers, $row['FollowID']);
        }
        $in = str_repeat('?,', count($followers)-1) . '?';

        $sql = "SELECT * FROM tblUploads INNER JOIN tblUsers ON tblUploads.userId=tblUsers.userID WHERE tblUploads.userID IN ($in) ORDER BY uploadId DESC LIMIT $p_limit,2";
        $stmt = $conn->prepare($sql);

        $stmt->execute($followers);
        $UploadRow = $stmt->fetchAll();
        return $UploadRow;
    }
    public function getSinglePhoto($p_photoID){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblUploads WHERE uploadID=:photoID");
        $stmt->bindparam(":photoID", $p_photoID);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getFilter($p_photoID){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT filter FROM tblUploads WHERE uploadID=:photoID");
        $stmt->bindparam(":photoID", $p_photoID);
        $stmt->execute();
        $result=$stmt->fetch(PDO::FETCH_ASSOC);
        return $result['filter'];
    }

    public function getUserPhotoList($p_userID){

        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblUploads WHERE userID=:userID");
        $stmt->bindparam(":userID", $p_userID);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function Like()
    {
        // AN EXAMPLE WITH PDO so that you can compare with mysqli syntax
        $conn = Db::getInstance();
        // here we get the current number of likes, so that we can add one to it

        // let's update the number of likes with the new number
        $stmt = $conn->prepare("INSERT INTO tblLikes(uploadId, userId)
                                                       VALUES(:uploadId, :userId)");
        $stmt->bindparam(":uploadId", $this->m_uploadid);
        $stmt->bindparam(":userId", $this->m_userid);
            $stmt->execute();


    }

    public function RemoveLike()
    {
        // AN EXAMPLE WITH PDO so that you can compare with mysqli syntax
        $conn = Db::getInstance();
        // here we get the current number of likes, so that we can add one to it

        // let's update the number of likes with the new number
        $stmt = $conn->prepare("DELETE FROM `tblLikes` WHERE `userId` =:userId AND `uploadId` =:uploadId");
        $stmt->bindparam(":uploadId", $this->m_uploadid);
        $stmt->bindparam(":userId", $this->m_userid);
        $stmt->execute();


    }

    public function getLikes($p_uploadId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblLikes WHERE uploadId=:uploadId");
        $stmt->bindparam(":uploadId", $p_uploadId);
        $stmt->execute();
        $count = $stmt->rowCount();
        return $count;
    }

    public function isLiked($p_uploadId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblLikes WHERE userId=:userId AND uploadId=:uploadId");
        $stmt->bindparam(":userId", $this->m_userid);
        $stmt->bindparam(":uploadId", $p_uploadId);
        $stmt->execute();
        $count = $stmt->rowCount();
        if($count>0){
            return true;
        }else{
            return false;
        }

    }
    public function deletePhoto($p_uploadId){
        $df = new Photo();
        $img = $df->getSinglePhoto($p_uploadId);
        $df->deleteCommentsOfPhoto($p_uploadId);
        $df->deleteLikesOfPhoto($p_uploadId);
        $df->deleteReportsOfPhoto($p_uploadId);
        $conn = Db::getInstance();
        $stmt = $conn->prepare("DELETE FROM `tblUploads` WHERE `uploadId` =:uploadId");
        $stmt->bindparam(":uploadId", $p_uploadId);
        if($stmt->execute()){
            $img = $img[0];
            $file = $img['photo'];
            unlink("../images/useruploads/$file");
            return true;
        }

    }

    public function deleteCommentsOfPhoto($p_uploadId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("DELETE FROM `tblComments` WHERE `uploadId` =:uploadId");
        $stmt->bindparam(":uploadId", $p_uploadId);
        $stmt->execute();
    }

    public function deleteLikesOfPhoto($p_uploadId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("DELETE FROM `tblLikes` WHERE `uploadId` =:uploadId");
        $stmt->bindparam(":uploadId", $p_uploadId);
        $stmt->execute();
    }

    public function deleteReportsOfPhoto($p_uploadId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("DELETE FROM `tblReports` WHERE `uploadId` =:uploadId");
        $stmt->bindparam(":uploadId", $p_uploadId);
        $stmt->execute();
    }

    public function getComments($p_uploadId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblComments INNER JOIN tblUsers ON tblComments.userId=tblUsers.userId WHERE uploadId=:uploadId ORDER BY  `tblComments`.`commentId` ASC");
        $stmt->bindparam(":uploadId", $p_uploadId);
        $stmt->execute();
        $CommentRow=$stmt->fetchAll();
        return $CommentRow;
    }

    public function reportPhoto($p_uploadId, $p_userId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblReports WHERE uploadId=:uploadId AND userId=:userId");
        $stmt->bindparam(":uploadId", $p_uploadId);
        $stmt->bindparam(":userId", $p_userId);
        $stmt->execute();
        $count = $stmt->rowCount();
        if($count == 0){
            $stmt = $conn->prepare("INSERT INTO tblReports(uploadId, userId)
                                                       VALUES(:uploadId, :userId)");
            $stmt->bindparam(":uploadId", $p_uploadId);
            $stmt->bindparam(":userId", $p_userId);
            $stmt->execute();
        }
        $stmt = $conn->prepare("SELECT * FROM tblReports WHERE uploadId=:uploadId");
        $stmt->bindparam(":uploadId", $p_uploadId);
        $stmt->execute();
        $count = $stmt->rowCount();
        if($count == 3){
            $stmt = $conn->prepare("DELETE FROM `tblUploads` WHERE `uploadId` =:uploadId");
            $stmt->bindparam(":uploadId", $p_uploadId);
            $stmt->execute();
        }

    }

    public function getTag($tag){
        $newtag = "%".$tag."%";

        $conn = Db::getInstance();

        $stmt = $conn->prepare("SELECT * FROM tblUploads INNER JOIN tblUsers ON tblUploads.userId=tblUsers.userID WHERE `description` LIKE :tag AND `private`= 0 ");
        $stmt->bindparam(":tag", $newtag);
        $stmt->execute();
        $tagRow=$stmt->fetchAll();

        return $tagRow;

    }
    public function getTagRows($tag){
        $newtag = "%".$tag."%";
        $conn = Db::getInstance();

        $stmt = $conn->prepare("SELECT * FROM tblUploads INNER JOIN tblUsers ON tblUploads.userId=tblUsers.userID WHERE `description` LIKE :tag AND `private`= 0");
        $stmt->bindparam(":tag", $newtag);
        $stmt->execute();
        $count = $stmt->rowCount();

        return $count;

    }

    function timeConverter($time)
    {
        $time_ago = strtotime($time);
        $cur_time   = time();
        $cur_time = $cur_time - 6*3600;
        //dit fixt de foute tijd...
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "1 year";
            }else{
                return "$years years ago";
            }
        }
    }


}

?>