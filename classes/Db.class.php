<?php

class Db
{
	private static $conn;

	public static function getInstance(){
		if( is_null( self::$conn ) ){
            try{
                self::$conn = new PDO("mysql:host=80.240.138.96; dbname=imdstagram_main", "imdstagram", "imdstagram");
            } catch(PDOException $e){
                echo $e->getMessage();
            }
		}
		return self::$conn;
	}
}

?>