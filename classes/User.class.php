<?php
include_once "Db.class.php";
class User{
    private $m_sUsername;
    private $m_sEmail;
    private $m_sFirstname;
    private $m_sLastname;
    private $m_sPassword;
    private $m_sSex;
    private $m_sBiography;
    private $m_sWebsite;
    private $m_sImage;
    private $m_iPosts;
    private $m_iFollowers;
    private $m_iFollowing;
    private $m_bPrivate;


    function __SET($p_sProperty, $p_vValue){
        switch ($p_sProperty){
            case "Username":
                $this->m_sUsername = $p_vValue;
                break;
            case "Email":
                $this->m_sEmail = $p_vValue;
                break;
            case "Firstname":
                $this->m_sFirstname = $p_vValue;
                break;
            case "Lastname":
                $this->m_sLastname = $p_vValue;
                break;
            case "Password":
                $this->m_sPassword = $p_vValue;
                break;
            case "Sex":
                $this->m_sSex = $p_vValue;
                break;
            case "Biography":
                $this->m_sBiography = $p_vValue;
                break;
            case "Website":
                $this->m_sWebsite = $p_vValue;
                break;
            case "Image":
                $this->m_sImage = $p_vValue;
                break;
            case "Posts":
                $this->m_iPosts = $p_vValue;
                break;
            case "Followers":
                $this->m_iFollowers = $p_vValue;
                break;
            case "Following":
                $this->m_iFollowing = $p_vValue;
                break;
            case "Private":
                $this->m_bPrivate = $p_vValue;
                break;
        }
    }

    function __GET($p_sProperty){
        switch ($p_sProperty){
            case "Username":
                return $this->m_sUsername;
                break;
            case "Email":
                return $this->m_sEmail;
                break;
            case "Firstname":
                return $this->m_sFirstname;
                break;
            case "Lastname":
                return $this->m_sLastname;
                break;
            case "Password":
                return $this->m_sPassword;
                break;
            case "Sex":
                return $this->m_sSex;
                break;
            case "Biography":
                return $this->m_sBiography;
                break;
            case "Website":
                return $this->m_sWebsite;
                break;
            case "Image":
                return $this->m_sImage;
                break;
            case "Posts":
                return $this->m_iPosts;
                break;
            case "Followers":
                return $this->m_iFollowers;
                break;
            case "Following":
                return $this->m_iFollowing;
                break;
            case "Private":
                return $this->m_bPrivate;
                break;
        }
    }

    public function updateSQL($p_sProperty, $p_vValue, $p_iUserId){
        $conn = Db::getInstance();
        switch ($p_sProperty){
            case "Username":
                $statement = $conn->prepare(" UPDATE tblUsers SET username = :username WHERE userId=:userId");
                $statement->bindValue(":username", $p_vValue);
                break;
            case "Email":
                $statement = $conn->prepare(" UPDATE tblUsers SET email = :email WHERE userId=:userId");
                $statement->bindValue(":email", $p_vValue);
                break;
            case "Firstname":
                $statement = $conn->prepare(" UPDATE tblUsers SET firstname = :firstname WHERE userId=:userId");
                $statement->bindValue(":firstname", $p_vValue);
                break;
            case "Lastname":
                $statement = $conn->prepare(" UPDATE tblUsers SET lastname = :lastname WHERE userId=:userId");
                $statement->bindValue(":lastname", $p_vValue);
                break;
            case "Password":
                $statement = $conn->prepare(" UPDATE tblUsers SET password = :password WHERE userId=:userId");
                $statement->bindValue(":password", $p_vValue);
                break;
            case "Sex":
                $statement = $conn->prepare(" UPDATE tblUsers SET sex = :sex WHERE userId=:userId");
                $statement->bindValue(":sex", $p_vValue);
                break;
            case "Biography":
                $statement = $conn->prepare(" UPDATE tblUsers SET biography = :biography WHERE userId=:userId");
                $statement->bindValue(":biography", $p_vValue);
                break;
            case "Website":
                $statement = $conn->prepare(" UPDATE tblUsers SET website = :website WHERE userId=:userId");
                $statement->bindValue(":website", $p_vValue);
                break;
            case "Image":
                $statement = $conn->prepare(" UPDATE tblUsers SET image = :image WHERE userId=:userId");
                $statement->bindValue(":image", $p_vValue);
                break;
            case "Posts":
                $statement = $conn->prepare(" UPDATE tblUsers SET posts = :posts WHERE userId=:userId");
                $statement->bindValue(":posts", $p_vValue);
                break;
            case "Followers":
                $statement = $conn->prepare(" UPDATE tblUsers SET followers = :followers WHERE userId=:userId");
                $statement->bindValue(":followers", $p_vValue);
                break;
            case "Following":
                $statement = $conn->prepare(" UPDATE tblUsers SET following = :following WHERE userId=:userId");
                $statement->bindValue(":following", $p_vValue);
                break;
            case "Private":
                $statement = $conn->prepare(" UPDATE tblUsers SET private = :private WHERE userId=:userId");
                $statement->bindValue(":private", $p_vValue);
                break;
        }
        $statement->bindValue(":userId", $p_iUserId);
        $statement->execute();
    }

    public function checkRegister(){
        if( empty($_POST['rEmail']) || empty($_POST['rFirstname']) || empty($_POST['rLastname']) ||
            empty($_POST['rUsername']) || empty($_POST['rPassword'])){
            return "<p class='red'>Gelieve alles in te vullen.</p>";
        } else if(!$this->emailIsUnique($this->Email)) {
            return "<p class='red'>$this->Email is al geregistreerd.</p>";
        } else if(!$this->usernameIsUnique($this->Username)){
            return "<p class='red'>De username '$this->Username' is al in gebruik.</p>";
        } else if( strlen($this->m_sPassword) < 5 ) {
            return "<p class='red'>Het paswoord moet langer dan 5 karakters zijn.</p>";
        } else if( !preg_match("#[0-9]+#", $this->m_sPassword) ) {
            return "<p class='red'>Het passwoord moet 1 cijfer bevatten.</p>";
        } else if( !preg_match("#[a-z]+#", $this->m_sPassword) ) {
            return "<p class='red'>Het passwoord moet 1 letter hebben.</p>";
        }else {
            return "succes";
        }
    }

    public function usernameIsUnique($p_username){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE username=:username");
        $stmt->bindparam(":username", $p_username);
        $stmt->execute();
        if($stmt->rowCount() !== 0){
            return false;
        }
        return true;
    }

    public function emailIsUnique($p_email){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE email=:email");
        $stmt->bindparam(":email", $p_email);
        $stmt->execute();
        if($stmt->rowCount() !== 0){
            return false;
        }
        return true;
    }

    public function register(){
        $feedback = $this->checkRegister();
        if($feedback === "succes") {

            try {
                $new_password = password_hash($this->m_sPassword, PASSWORD_DEFAULT);
                $conn = Db::getInstance();
                $stmt = $conn->prepare("INSERT INTO tblUsers(username, email, firstname, lastname, password, private)
                                        VALUES(:username, :email, :firstname, :lastname, :password, false)");
                $stmt->bindparam(":username", $this->m_sUsername);
                $stmt->bindparam(":email", $this->m_sEmail);
                $stmt->bindparam(":firstname", $this->m_sFirstname);
                $stmt->bindparam(":lastname", $this->m_sLastname);
                $stmt->bindparam(":password", $new_password);

                if ($stmt->execute()) {
                    //je eigen volgen voor eigen posts te zien
                    $stmt = $conn->prepare("SELECT `userId` FROM `tblUsers` WHERE `username` = :username;");
                    $stmt->bindparam(":username", $this->m_sUsername);
                    $stmt->execute();
                    $result = $stmt->fetch(PDO::FETCH_ASSOC);
                    $id = $result['userId'];
                    //id fetchen op basis van username (deze moeten toch uniek zijn)

                    //id's inserten bij followers
                    $stmt = $conn->prepare("INSERT INTO `tblFollowers`(`userID`, `FollowID`) VALUES (:id, :id)");
                    $stmt->bindparam(":id", $id);
                    if($stmt->execute()){
                        return "succes";
                    }
                }
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        }else{
            return $feedback;
        }
    }

    public function login($p_username, $p_password){
        try{
            $conn = Db::getInstance();
            $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE username=:username");
            $stmt->bindparam(":username", $p_username);
            $stmt->execute();
            $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
            if($stmt->rowCount() > 0){
                if(password_verify($p_password, $userRow['password'])){
                    $_SESSION['id'] = $userRow['userId'];
                    $_SESSION['username'] = $userRow['username'];
                    $_SESSION['loggedin'] = true;
                    return true;
                }
                else{
                    return false;
                }
            }
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function userExists($p_username){
        try{
            $conn = Db::getInstance();
            $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE username=:username");
            $stmt->bindparam(":username", $p_username);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return true;
            }else{
                return false;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function correctPassword($p_username, $p_password){
        try{
            $conn = Db::getInstance();
            $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE username=:username");
            $stmt->bindparam(":username", $p_username);
            $stmt->execute();
            $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
            if($stmt->rowCount() > 0){
                if(password_verify($p_password, $userRow['password'])){
                    return true;
                }
                else{
                    return false;
                }
            }
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function getUserIdWithUsername($p_username){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT userId FROM tblUsers WHERE username=:username");
        $stmt->bindparam(":username", $p_username);
        $stmt->execute();
        $result=$stmt->fetch(PDO::FETCH_ASSOC);
        return $result['userId'];
    }

    public function getUsernameWithUserID($p_userID){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT username FROM tblUsers WHERE userID=:userID");
        $stmt->bindparam(":userID", $p_userID);
        $stmt->execute();
        $result=$stmt->fetch(PDO::FETCH_ASSOC);
        return $result['username'];
    }

    public function getImageWithUserID($p_userID){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT image FROM tblUsers WHERE userID=:userID");
        $stmt->bindparam(":userID", $p_userID);
        $stmt->execute();
        $result=$stmt->fetch(PDO::FETCH_ASSOC);
        return $result['image'];
    }

    public function followersCount($p_userId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT userID FROM tblFollowers WHERE FollowID=:FollowID");
        $stmt->bindparam(":FollowID", $p_userId);
        $stmt->execute();
        $PhotoRow=$stmt->fetchAll();
        $result = count($PhotoRow)-1;
        return $result;
    }

    public function followingCount($p_userId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT FollowID FROM tblFollowers WHERE userID=:userID");
        $stmt->bindparam(":userID", $p_userId);
        $stmt->execute();
        $PhotoRow=$stmt->fetchAll();
        $result = count($PhotoRow)-1;
        return $result;
    }

    public function postsCount($p_userId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblUploads WHERE userID=:userID");
        $stmt->bindparam(":userID", $p_userId);
        $stmt->execute();
        $PhotoRow=$stmt->fetchAll();
        $result = count($PhotoRow);
        return $result;
    }

    public function getUserByUsername($p_username){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE username=:username");
        $stmt->bindparam(":username", $p_username);
        $stmt->execute();
        $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
        $this->__SET("Username", $userRow['username']);
        $this->__SET("Email", $userRow['email']);
        $this->__SET("Firstname", $userRow['firstname']);
        $this->__SET("Lastname", $userRow['lastname']);
        $this->__SET("Private", $userRow['private']);
        if(isset($userRow['image'])){
            $this->__SET("Image", $userRow['image']);
        }
        if(isset($userRow['sex'])){
            $this->__SET("Sex", $userRow['sex']);
        }
        if(isset($userRow['biography'])){
            $this->__SET("Biography", $userRow['biography']);
        }
        if(isset($userRow['website'])){
            $this->__SET("Website", $userRow['website']);
        }
    }

    static function isPrivate($p_userID)
    {
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT private FROM tblUsers WHERE userID=:userID");
        $stmt->bindparam(":userID", $p_userID);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result['private'] == 1) {
            return true;
        }else{
            return false;
        }
    }

    public function editUserForm($p_username, $p_email){
        if (htmlspecialchars($_POST['profile_username']) !== $p_username) {
            if ($this->usernameIsUnique(htmlspecialchars($_POST['profile_username']))) {
                $this->updateSQL("Username", htmlspecialchars($_POST['profile_username']), $_SESSION['id']);
                $_SESSION['username'] = htmlspecialchars($_POST['profile_username']);
            } else {
                return "Gebruikersnaam is al in gebruik.";
            }
        }

        if ($_POST['profile_email'] !== $p_email){
            if($this->emailIsUnique(htmlspecialchars($_POST['profile_email']))) {
                $this->updateSQL("Email", htmlspecialchars($_POST['profile_email']), $_SESSION['id']);
            }else{
                return "Email is al in gebruik.";
            }
        }

        $this->updateSQL("Firstname", htmlspecialchars($_POST['profile_firstname']), htmlspecialchars($_SESSION['id']));
        $this->updateSQL("Lastname", $_POST['profile_lastname'], $_SESSION['id']);

        if (isset($_POST['profile_bio'])) {
            $this->updateSQL("Biography", htmlspecialchars($_POST['profile_bio']), $_SESSION['id']);
        }
        if (isset($_POST['profile_sex'])) {
            $this->updateSQL("Sex", htmlspecialchars($_POST['profile_sex']), $_SESSION['id']);
        }
        if (isset($_POST['profile_website'])) {
            $this->updateSQL("Website", htmlspecialchars($_POST['profile_website']), $_SESSION['id']);
        }
        if (isset($_POST['profile_private'])) {
            $private = true;
            $this->updateSQL("Private", $private, $_SESSION['id']);
        }else{
            $private = false;
            $this->updateSQL("Private", $private, $_SESSION['id']);
        }
        return false;
    }

}
?>