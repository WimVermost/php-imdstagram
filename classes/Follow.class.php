<?php
include_once "Db.class.php";
class Follow
{
    private $m_iUserId;
    private $m_iFollowId;

    function __SET($p_sProperty, $p_vValue)
    {
        switch ($p_sProperty) {
            case "userId":
                $this->m_iUserId = $p_vValue;
                break;
            case "followId":
                $this->m_iFollowId = $p_vValue;
                break;
        }
    }
    function __GET($p_sProperty)
    {
        switch ($p_sProperty) {
            case "userId":
                return $this->m_iUserid;
                break;
            case "followId":
                return $this->m_iFollowId;
                break;
        }
    }
    public function followPerson()
    {
        $conn = Db::getInstance();
        $stmt = $conn->prepare("INSERT INTO `tblFollowers`(`userID`, `FollowId`) VALUES (:userId, :followId)");
        $stmt->bindparam(":userId", $this->m_iUserId);
        $stmt->bindparam(":followId", $this->m_iFollowId);
        return $stmt->execute();
    }
    public function unfollowPerson(){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("DELETE FROM `tblFollowers` WHERE `userID`=:userId and `FollowId`=:followId");
        $stmt->bindparam(":userId", $this->m_iUserId);
        $stmt->bindparam(":followId", $this->m_iFollowId);
        return $stmt->execute();
    }
    public function isFollowing(){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM `tblFollowers` WHERE `userID`=:userId and `FollowId`=:followId");
        $stmt->bindparam(":userId", $this->m_iUserId);
        $stmt->bindparam(":followId", $this->m_iFollowId);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }else{
            return false;
        }
    }

    static function setFollowRequest($p_userId, $p_followId){
        if(!Follow::hasSubmitedRequest($p_userId, $p_followId)) {
            try {
                $conn = Db::getInstance();
                $stmt = $conn->prepare("INSERT INTO `tblFollowRequests` (`userId`, `followId`) VALUES (:userId, :followId)");
                $stmt->bindparam(":userId", $p_userId);
                $stmt->bindparam(":followId", $p_followId);
                $stmt->execute();
            } catch (PDOException $e) {
                return "failed";
            }
        }
    }

    static function hasSubmitedRequest($p_userId, $p_followId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM `tblFollowRequests` WHERE `userId`=:userId and `followId`=:followId");
        $stmt->bindparam(":userId", $p_userId);
        $stmt->bindparam(":followId", $p_followId);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return true;
        }else{
            return false;
        }
    }

    static function countFollowRequests($p_followId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM `tblFollowRequests` WHERE `followId`=:followId");
        $stmt->bindparam(":followId", $p_followId);
        $stmt->execute();
        return $stmt->rowCount();
    }

    static function getFollowRequests($p_followId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM `tblFollowRequests` WHERE `followId`=:followId");
        $stmt->bindparam(":followId", $p_followId);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    static function acceptRequest($p_userId, $p_followId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("INSERT INTO `tblFollowers`(`userID`, `FollowId`) VALUES (:userId, :followId)");
        $stmt->bindparam(":userId", $p_userId);
        $stmt->bindparam(":followId", $p_followId);
        return $stmt->execute();
    }

    static function removeRequest($p_userId, $p_followId){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("DELETE FROM `tblFollowRequests` WHERE `userId`=:userId and `followId`=:followId");
        $stmt->bindparam(":userId", $p_userId);
        $stmt->bindparam(":followId", $p_followId);
        $stmt->execute();
    }
}