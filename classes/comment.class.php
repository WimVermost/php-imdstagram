<?php
include_once "Db.class.php";
class Comment
{
    private $m_sComment;
    private $m_iUploadId;
    private $m_iUserid;


    function __SET($p_sProperty, $p_vValue)
    {
        switch ($p_sProperty) {
            case "Comment":
                $this->m_sComment = $p_vValue;
                break;
            case "Uploadid":
                $this->m_iUploadId = $p_vValue;
                break;
            case "Userid":
                $this->m_iUserid = $p_vValue;
                break;
        }
    }

    function __GET($p_sProperty)
    {
        switch ($p_sProperty) {
            case "Comment":
                return $this->m_sComment;
                break;
            case "Userid":
                return $this->m_iUserid;
                break;
            case "Uploadid":
                return $this->m_iUploadId;
                break;
        }

    }

    function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    function endsWith($haystack, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }

    function covertComment($p_comment){
        $commentSplit = explode(" ",$p_comment);

        $newString = "";
        foreach($commentSplit as $value){
            if($this->startsWith($value, "#")){
                $name = str_replace("#","",$value);
                $newValue = "<a href='tag.php?tag=$name'>$value</a>";
            }elseif($this->startsWith($value,"@")){
                $name = str_replace("@","",$value);
                $newValue = "<a href='profile.php?user=$name'>$value</a>";
            }else{
                $newValue = $value;
            }

            $newString .= $newValue." ";
        }
        return $newString;
    }

    public function placeComment()
    {
        $comment = $this->covertComment($this->m_sComment);
        $conn = Db::getInstance();

        $stmt = $conn->prepare("INSERT INTO tblComments(uploadId, userId, comment)
                                                       VALUES(:uploadId, :userId, :comment)");
        $stmt->bindparam(":uploadId", $this->m_iUploadId);
        $stmt->bindparam(":userId", $this->m_iUserid);
        $stmt->bindparam(":comment", $comment);
        $stmt->execute();


    }
}