<?php
    include_once "classes/User.class.php";
    session_start();
    if(isset($_SESSION['id'])){
        header('location: index.php');
    }

    if(isset($_POST['loginSubmit'])) {

        $user = new User();
        $username = htmlspecialchars($_POST['loginUsername']);
        $password = htmlspecialchars($_POST['loginPassword']);

        if($user->login($username, $password)){
            header('location: index.php');
        }
        else{
            $feedback = "<p class='red'> Login mislukt, probeer opnieuw. </p>";
        }

    }

    if(isset($_SESSION['register'])){
        $feedback = $_SESSION['register'];
        session_destroy();
    }

?><!DOCTYPE html>
<html lang="en">
<head>
    <title>Login - IMDstagram</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.png" sizes="16x16 32x32" type="image/png">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-2.2.3.min.js" defer></script>
    <script src="js/typeahead.min.js" defer></script>
    <script src="js/imd.js" defer></script>
</head>

<body>
    <style>nav{height:70px}</style>
    <?php include_once "includes/nav.php"?>
    <section class="form">
        <a href="index.php" id="mainLogo">Logo</a>
        <form action="" method="POST">
            <?php
            if(isset($feedback)) {
                echo "$feedback";
            }else{
                echo "<p> Welkom terug! Login </p>";
            }
            ?>
            <input name="loginUsername" class="formInput" type="text" placeholder="Gebruikersnaam">
            <input name="loginPassword" class="formInput" type="password" placeholder="Wachtwoord">
            <input type="submit" id="loginSubmit" name="loginSubmit" value="Login">
            <span>Nog geen account? <a href="register.php">Registreren</a></span>
        </form>
    </section>
    <?php include_once "includes/footer.php"?> 
</body>
</html>