<?php
    include_once('classes/Photo.class.php');
    include_once('classes/User.class.php');
    //$Photo = new Photo();
    session_start();
    if(!isset($_SESSION['id'])){
        header('location: login.php');
    }

    include_once('includes/photohandler.php');

    //check if load more has to be displayed
    $ph = new Photo();
    $ph->Userid = $_SESSION['id'];
    $count = count($ph->getFullPhotoList());

    $displayLoadMore = true;
    if($count < 3){
        $displayLoadMore = false;
    }

?><!doctype html>
<html lang="en">
<head>
    <title>IMDstagram</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.png" sizes="16x16 32x32" type="image/png">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/dropzone.min.css">
    <link rel="stylesheet" href="css/cssgram.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-2.2.3.min.js" defer></script>
    <script src="js/typeahead.min.js" defer></script>
    <script src="js/imd.js" defer></script>
</head>
<body>
   <?php include "includes/nav.php"; ?>
   <div id="createPost">
        <h2 class="createPostHeader">
            <?php if(isset($message)){
              echo $message;
            } else{ ?>
            Klik hier om een post te plaatsen
            <?php } ?></h2>
      <form id="imageform" method="post" enctype="multipart/form-data" action='index.php'>
          <input type="hidden" id="activeUser" value="<?php echo $activeUser; ?>">
          <input type="hidden" id="lat" name="lat" value="">
          <input type="hidden" id="long" name="long" value="">
          <label for="photoimg" class="custom-file-upload">Selecteer een foto</label>
          <input type="file" name="photoimg" id="photoimg" class="choosefilebutton"/>
          <div id="filter-box"><img id="pre-vieuw" src="#" alt="your image" /></div>
          <textarea name="userdescription" id="userdescription" class="userdescription" cols="30" rows="10" maxlength="80" placeholder="Beschrijving"></textarea>
          <div class="createPostwrapper">
          <select name="filter" id="filter_select">
              <option value="">#nofilter</option>
              <option value="_1977">1977</option>
              <option value="aden">Aden</option>
              <option value="brooklyn">Brooklyn</option>
              <option value="clarendon">Clarendon</option>
              <option value="earlybird">Earlybird</option>
              <option value="gingham">Gingham</option>
              <option value="hudson">Hudson</option>
              <option value="inkwell">Inkwell</option>
          </select>
          <input type="submit" name="createPost" id="postbutton" value="foto posten">
         </div>
       </form>
    </div>
    <div class="photo-reported">
        <h4>Bedankt voor de feedback</h4>
    </div>
    <div class="index-article article">
        <?php
        $Photo = new Photo();
        $Photo->Userid = $_SESSION['id'];
        $Loads = $Photo->getPhotoList();
        foreach($Loads as $x){?>
        <article>
            <div class="title">
                <a class="user-info" href="profile.php?user=<?php echo $x['username'] ?>">
                    <div class="profile_picture_small"><img class="profile_image" src="images/profilepictures/<?php echo $x['image']; ?>" alt="Photo of <?php echo $x['username']; ?>"></div>
                    <div><h3><?php echo $x['username']; ?></h3></div>
                </a>
                <div>
                    <h4><?php echo $Photo->timeConverter($x['timeposted']); ?></h4>
                </div>
            </div>
            <?php
            $filter = $x['filter'];
            ?>
            <div class="image<?php if(!empty($filter)){echo" $filter";} ?>">
                <img src="images/useruploads/<?php echo $x['photo'];?>" alt="">
            </div>
            <?php if($x['latitude']!= ""){ ?>
            <div class="loc"><a href="#" class="locIcon">Icon</a><?php echo $Photo->get_location($x['latitude'],$x['longitude']) ?></div><?php } ?>
            <div class="content">
                <div class="likes" id="like_<?php echo $x['uploadId']; ?>">
                    <?php
                    $likes = $Photo->getLikes($x['uploadId']);
                    echo $likes. " Likes";
                    ?>
                </div>
                <div class="comment">
                    <p><a class="username" href="profile.php?user=<?php echo $x['username']; ?>"><?php echo $x['username'];?></a>
                    <?php echo $x['description']; ?></p>
                </div>

                <!-- loop comments -->
                <div id="commentContainer_<?php echo $x['uploadId'];?>">
                <?php
                $comm = $Photo->getComments($x['uploadId']);
                foreach($comm as $y){
                ?>
                <div class="comment">
                    <p><a class="username" href="profile.php?user=<?php echo $y['username']; ?>"><?php echo $y['username'];?></a>
                    <?php echo $y['comment'];?></p>
                </div>

                <?php }
                ?>
                </div>
                <div class="content-footer">
                    <div class="btn-like"><img class="like" id="<?php echo $x['uploadId'] ?>" src="<?php
                        if($Photo->isLiked($x['uploadId'])){
                            echo "images/heart.png";
                        }else{
                            echo "images/heart_line.png";
                        }
                        ?>" alt=""></div>
                    <div class="post">
                            <input id='<?php echo $x['uploadId']; ?>' name="<?php echo $x['uploadId']; ?>" type="text" placeholder="Add a comment..." class="placecomment">
                    </div>
                    <div class="option" id="report_<?php echo $x['uploadId']; ?>"><img src="images/more.png" alt=""></div>
                </div>
            </div>
        </article>
        <?php }
        ?>
    </div>
   <?php if($displayLoadMore) {
       echo"<div id = 'offset_2'  class='btn-loadmore' ><span > Load More </span ></div >";
   }?>
   <div class="modal report">
       <div class="modal-content">

           <p>Reason:</p>
           <textarea placeholder="Optional..."></textarea>
           <div class="modal-content-btn">
               <a href="#" id="sendReport">Report inappropriate</a>
               <a href="#" id="btn-report-cancel">Cancel</a>
           </div>

       </div>

   </div>
    <?php include_once "includes/footer.php"?>
</body>
</html>